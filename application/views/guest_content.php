

<div class="container"> 
      <div class="row">
        <div class="col-lg-8 ">
          <h2 class="text-center">Recent status updates</h2><hr/>
          <ul class="list-unstyled ">
              <?php foreach($data as $row){
                if($row['URL'] == NULL && $row['publicstatus']){?>
                <li id="<?php echo $row['id']?>"class="well">
                    <article>
                        <div class="media">
                            <a href="<?php echo base_url().'index.php/welcome/profile_guest_view/'.$row['userid'] ?>" class="pull-left text-center"><img src="<?php echo $row['picture']?>" class="img-circle media-object" width="100" height="100"><h5><?php echo $row['FirstName'].' '.$row['LastName']; ?></h5></a>
                            <div class="media-body">
                            <div class="row">
                                <div class="col-lg-8">
                                    <h3 class="media-heading"><?php $this->load->model('status');
                        echo $this->status->replace_hashtags($row['content']); ?></h3>
                                   <ul class="list-inline">
                                    <li>
                                    <span  class="numlikes glyphicon glyphicon-time text-muted"><?php echo ' '.$row['date'];?></span>
                                    </li>
                                    <li>
                                    <span id="numlikes" class="glyphicon glyphicon-hand-up text-muted"><?php echo $row['numlikes']; ?></span>
                                     </li>
                                 </ul>
                                <br>
                    
                                </div>
                            <div class="col-lg-4">
                                <ul class="list-unstyled pull-right">
                                <li class="pull-right">
                                <p><button id="<?php echo $row['id']?>" type="button" class="btn btn-info disabled"><span class="glyphicon glyphicon-hand-up"></span> Support</button></p>
                                </li>
                                <li class="pull-right">
                                    <p><button type="button" class="btn btn-warning disabled" ><span class="glyphicon glyphicon-envelope"></span>Send message</button></p>
                                </li>
                                </ul>
                                
                            </div>
                           </div>
                    
                   
                    
                  </div>
                </div>
              </article>
            </li>
                <?php }}?>
          </ul>
        </div>
          <div class="col-lg-4">
            <h2 class="text-center">Recent image updates</h2><hr/>
            <ul class="list-unstyled">
            <?php foreach ($data as $row) {
                if ($row['URL'] != NULL && $row['publicphotos']) { ?>
                    <li id="<?php echo $row['id'] ?>"class="well">
                        <div class="media">
                            <div class="thumbnail">
                                <img src="<?php echo $row['URL']; ?>">
                                <div class="caption">
                                    <div>
                                        <img src="<?php echo $row['picture'] ?>" class="img-circle pull-right" width="50" height="50">
                                        <p class="pull-right"><a href="<?php echo base_url() . 'index.php/welcome/profile/' . $row['userid'] ?>"><?php echo $row['FirstName'] . ' ' . $row['LastName']; ?></a></p>
                                    </div>

                                    <h3><?php $this->load->model('status');
                        echo $this->status->replace_hashtags($row['content']); ?></h3>
                                    <ul class="list-inline">
                                        <li>
                                            <span  class="numlikes glyphicon glyphicon-time text-muted"><?php echo ' ' . $row['date']; ?></span>
                                        </li>
                                        <li>
                                            <span id="numlikes" class="glyphicon glyphicon-hand-up text-muted"><?php echo $row['numlikes']; ?></span>
                                        </li>
                                    </ul>
                                    <br>
                                    <p>
                                        <button id="<?php echo $row['id'] ?>" type="button" class="btn btn-info disabled"><span class="glyphicon glyphicon-hand-up"></span> Support</button>
                                        <?php if($row['userid'] != $this->session->userdata('id')){ ?>
                                        <button type="button" class="btn btn-warning send_msg disabled" data-toggle="modal" data-target="#myModalcompose"  data-id="<?php echo $row['userid']; ?>" data-fname="<?php echo $row['FirstName']; ?>" data-lname="<?php echo $row['LastName']; ?>"><span class="glyphicon glyphicon-envelope" data-toggle="modal" data-target="#myModalcompose" ></span>Send message</button>
                                        <?php } ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php }
            } ?>
            </ul>
        </div>
      </div>
</div>

<div class="stupar">
    
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript">
    $('.btn.btn-info').click(function(){
       var id=$(this).attr('id');
       var elem=$(this).closest(".media").find('#numlikes').text();
       $(this).addClass('disabled');
       elem++;
       elem=$(this).closest(".media").find('#numlikes').text(elem);
       $.ajax({
           'url':"<?php echo base_url().'index.php/welcome/support/'?>",
           'type': 'POST',
           'data':{'id':id},
           'success':function(msg){
               //$('.stupar').html(msg);
               var idd=1;
              
           }
       });
    });
    
    
    function numLikes(id){
       
    }
</script>