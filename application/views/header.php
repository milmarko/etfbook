<!--Autor: Jovan Stupar-->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ETF book</title>

        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/additional.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <header class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-contents">  
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?php echo base_url() ?>index.php/welcome/members" class="navbar-brand"><b>ETFbook</b> <?php echo ' | ' . $this->session->userdata('FirstName') . ' ' . $this->session->userdata('LastName'); ?></a>
                </div>

                <div class="collapse navbar-collapse" id="navbar-contents">
                    <ul class="nav navbar-nav" role="navigation">
                        <p class="navbar-text"></p>
                        <li><a href="<?php echo base_url() ?>index.php/welcome/members"> <span class="glyphicon glyphicon-home"></span>  Home</a></li>
                        <li><a href="<?php echo base_url() ?>index.php/welcome/my_profile/<?php echo $this->session->userdata('id'); ?>"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
                        <li><a href="<?php echo base_url() ?>index.php/welcome/messages"><span class="glyphicon glyphicon-envelope"></span>  Messages</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                         <li><a href="<?php echo base_url() ?>index.php/welcome/list_all_users"><span class="glyphicon glyphicon-list"></span>  All Users</a></li>
                        <li><a href="<?php echo base_url() ?>index.php/welcome/settings"><span class="glyphicon glyphicon-cog"></span>  Settings</a></li>
                        <!--<li><a href="#">About</a></li>-->   
                        <li><a href="<?php echo base_url() ?>index.php/welcome/logout"><span class="glyphicon glyphicon-log-out"></span>  Logout</a></li>
                    </ul>
                    <!--<form class="navbar-form  navbar-right" role="search">-->
                    <ul class="nav navbar-nav navbar-right">
                        <li><?php echo form_open_multipart('welcome/search'); ?>
                        <fieldset>
                                <div class="form-group">
                                <?php
                                    $data = array(
                                        'name' => 'search_text',
                                        'id' => 'search_text',
                                        'placeholder' => 'Search',
                                        'class' => 'form-control',
                                        'rows' => '1',
                                        'maxlength' => "140",
                                        'style' => 'margin:10px'
                                    );
                                    echo form_input($data);
                                   ?>
                                    </div>
                                <!--<input type="text" id ="search_text" class="form-control" placeholder="Search"> 
                            <button type="submit" id = "search_btn" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>-->
                        <!--<input  class="btn btn-default navbar-right" type="submit" value="Search" >-->
                            </fieldset>
                        <?php echo form_close(); ?>
                        </li>
                    </ul>
                   <!-- </form>-->
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-file"></span>What's new?</a></li>
                    </ul>
                </div>
            </div>
        </header>

        <div class="container text-center">
            <!-- Button trigger modal -->
            <br>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">What's new?</h4>
                        </div>
                        <div class="modal-body">
                            <?php echo form_open_multipart('welcome/new_status'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <?php
                                    $data = array(
                                        'name' => 'status',
                                        'id' => 'status',
                                        'placeholder' => 'Write your new status...',
                                        'class' => 'form-control',
                                        'rows' => '6',
                                        'maxlength' => "140",
                                    );
                                    echo form_textarea($data);
                                    ?>                                    
                                    <input style="margin:10px;" type="file" name="userfile" size="20"/>
                                </div>
                                <br><br>
                                <input class="btn btn-lg btn-success btn-block vertical-offset-100" type="submit" value="Post">
                            </fieldset>
                            <?php echo form_close(); ?>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
