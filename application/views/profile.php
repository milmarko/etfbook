<link href="<?php echo base_url(); ?>assets/profile.css" rel="stylesheet">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel" style="background-image: url(<?php echo $user['cover']?>)" ?>
                <img class="pic img-circle" src="<?php echo $user['picture']; ?>" width="120" height="120" alt="...">
                <div class="name"><small><?php echo $user['FirstName'] . " " . $user['LastName']; ?></small></div> 
            </div>            

            <br><br><br>           
            
            <?php if ($user['id'] != $my_id) { ?>
                <ul class="nav nav-pills" id="myTab">
                    <p>
                        <?php if (in_array($this->session->userdata('id'), $followers) != true) { ?>
                        <button id="<?php echo $user['id'] ?>" type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus"></span>
                             Follow
                        </button>
                        <?php } else { ?>
                        <button id="<?php echo $user['id'] ?>" type="button" class="btn btn-danger">
                            <span class="glyphicon glyphicon-minus"></span>
                             Unfollow
                        </button>
                        <?php } ?>
                    </p>
                </ul>
                <br>
            <?php }
            else {?>
            <div class="container">
                <div class="container">
                        <div class="col-md-12">
                            <button class="btn btn-xs btn-primary pull-left" id="uploadbtn" style="margin:10px;" data-toggle="modal" data-target="#uploadModal">
                                <span class="glyphicon glyphicon-picture"></span>
                                Change profile photo 
                            </button>
                        </div>                    

                    <!-- Modal -->
                    <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" id="submitbtn" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Upload photo</h4>
                                </div>
                                <div class="modal-body">
                                    <?php
                                    echo form_open_multipart('upload/do_upload');
                                    ?>                                  
                                    <label for="userfile">Change profile photo</label>
                                    <fieldset>
                                         <input type="file" name="userfile" size="20"/>
                                        <br><br>
                                        <input class="btn btn-lg btn-success btn-block" type="submit" value="upload">
                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
                    <div class="container">
                        <div class="col-md-12">
                            <button class="btn btn-xs btn-primary pull-left" id="signupbtn" style="margin:10px;" data-toggle="modal" data-target="#coverModal">
                                <span class="glyphicon glyphicon-picture"></span>
                                Change cover photo
                            </button>
                        </div>
                    

                    <!-- Modal -->
                    <div class="modal fade" id="coverModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" id="submitbtn" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Upload photo</h4>
                                </div>
                                <div class="modal-body">
                                    <?php
                                    echo form_open_multipart('upload/do_upload_cover');
                                    ?>                                  
                                    <label for="userfile">Change cover photo</label>
                                    <fieldset>
                                         <input type="file" name="userfile" size="20"/>
                                        <br><br>
                                        <input class="btn btn-lg btn-success btn-block" type="submit" value="upload">
                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>

                <?php } ?>

            <div class="container"> 
                <div class="row">
                    <div class="col-lg-8 ">
                        <ul class="list-unstyled ">
                            <?php foreach ($status as $row) {
                                if ($row['URL'] == NULL) { ?>
                            
                                <li id="<?php echo $row['id'] ?>" class="well">
                                        <div class="media">
                                            <a href="#" class="pull-left text-center"><img src="<?php echo $user['picture'] ?>" class="img-circle media-object" width="100" height="100"><h5><?php echo $user['FirstName'] . ' ' . $user['LastName']; ?></h5></a>
                                            <div class="media-body">
                                                <div class="row">
                                                    <div class="col-lg-8">
                                                        <h3 class="media-heading"><?php $this->load->model('status'); echo $this->status->replace_hashtags($row['content']); ?></h3>
                                                        <ul class="list-inline">
                                                            <li>
                                                                <span  class="numlikes glyphicon glyphicon-time text-muted"><?php echo ' ' . $row['date']; ?></span>
                                                            </li>
                                                            <li>
                                                                <span id="numlikes" class="glyphicon glyphicon-hand-up text-muted"><?php echo $row['numlikes']; ?></span>
                                                            </li>
                                                        </ul>
                                                        <br>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <ul class="list-unstyled pull-right">
                                                            <li class="pull-right">
                                                                <p><button id="<?php echo $row['id'] ?>" type="button" class="btn btn-info <?php if (in_array($this->session->userdata('id'), $row['supports']) != false) echo "disabled"; ?>"><span class="glyphicon glyphicon-hand-up"></span> Support</button></p>
                                                            </li>
                                                            <?php if ($user['id'] != $my_id) { ?>
                                                                <li class="pull-right">
                                                                    <p><button type="button" class="btn btn-warning" ><span class="glyphicon glyphicon-envelope"></span>Send message</button></p>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </li>
                            <?php }} ?>
                        </ul>
                    </div>
                     <div class="col-lg-4">
                        <ul class="list-unstyled">
                        <?php foreach ($status as $row) {
                            if ($row['URL'] != NULL) { ?>
                                <li id="<?php echo $row['id'] ?>">
                                    <div class="media">
                                        <div class="thumbnail">
                                            <img src="<?php echo $row['URL']; ?>">
                                            <div class="caption">
                                                <div>
                                                    <img src="<?php echo $user['picture'] ?>" class="img-circle pull-right" width="50" height="50">
                                                    <p class="pull-right"><a href="<?php echo base_url() . 'index.php/welcome/profile/' . $user['id'] ?>"><?php echo $user['FirstName'] . ' ' . $user['LastName']; ?></a></p>
                                                </div>

                                                <h3><?php $this->load->model('status');
                                    echo $this->status->replace_hashtags($row['content']); ?></h3>
                                                <ul class="list-inline">
                                                    <li>
                                                        <span  class="numlikes glyphicon glyphicon-time text-muted"><?php echo ' ' . $row['date']; ?></span>
                                                    </li>
                                                    <li>
                                                        <span id="numlikes" class="glyphicon glyphicon-hand-up text-muted"><?php echo $row['numlikes']; ?></span>
                                                    </li>
                                                </ul>
                                                <br>
                                                <p>
                                                    <button id="<?php echo $row['id'] ?>" type="button" class="btn btn-info <?php if (in_array($this->session->userdata('id'), $row['supports']) != false) echo "disabled"; ?>"><span class="glyphicon glyphicon-hand-up"></span> Support</button>
                                                    <?php if($user['id'] != $this->session->userdata('id')){ ?>
                                                    <button type="button" class="btn btn-warning send_msg" data-toggle="modal" data-target="#myModalcompose"  data-id="<?php echo $row['id']; ?>" data-fname="<?php echo $user['FirstName']; ?>" data-lname="<?php echo $user['LastName']; ?>"><span class="glyphicon glyphicon-envelope" data-toggle="modal" data-target="#myModalcompose" ></span>Send message</button>
                                                    <?php } ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php }
                        } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript">
    $('.btn.btn-danger').click(function() {
        var id = $(this).attr('id');
        var url = "<?php echo base_url() . "index.php/welcome/unfollow/"; ?>";
        
        //alert(url);
        $.ajax({
            'url': url,
            'type': 'POST',
            'data': {'idf': id},
            'success': function(data) {
                if (data != null) {
                    location.reload(true);
                    var idd = 1;
                }
            }
        });
        // alert(id);
    });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript">
    $('.btn.btn-primary').click(function() {
        var id = $(this).attr('id');
        var url = "<?php echo base_url() . "index.php/welcome/follow/"; ?>";
        
        //alert(url);
        $.ajax({
            'url': url,
            'type': 'POST',
            'data': {'idf': id},
            'success': function(data) {
                if (data != null) {
                    location.reload(true);
                    var idd = 1;
                }
            }
        });
        // alert(id);
    });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript">
    $('.btn.btn-info').click(function() {
        var id = $(this).attr('id');
        var elem = $(this).closest(".media").find('#numlikes').text();
        $(this).addClass('disabled');
        elem++;
        elem = $(this).closest(".media").find('#numlikes').text(elem);
        $.ajax({
            'url': "<?php echo base_url() . 'index.php/welcome/support/' ?>",
            'type': 'POST',
            'data': {'id': id},
            'success': function(msg) {
                //$('.stupar').html(msg);
                var idd = 1;
            }
        });
    });


    function numLikes(id) {

    }
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script>
    $(function() {
        $('#myTab a:last').tab('show')
    })
</script>
</body>
</html>