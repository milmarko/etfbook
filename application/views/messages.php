
<link href="<?php echo base_url() ?>assets/profile.css" rel="stylesheet">

<div class="container">

    <div class="row well">
        <div class="col-md-12">
            <div class="panel" style="background-image: url(<?php echo $cover?>)">

                <img class="pic img-circle" src="<?php echo $this->session->userdata('picture'); ?>" alt="..." width="100" height="100">
                <div class="name"><small><?php echo $this->session->userdata('FirstName') . ' ' . $this->session->userdata('LastName'); ?></small></div>
                <a href="#" class="btn btn-xs btn-primary pull-right" style="margin:10px;"><span class="glyphicon glyphicon-picture"></span> Change cover</a>
            </div>

            <br><br><br>
            <ul class="nav nav-pills" id="myTab">
                <li class="active"><a href="#inbox" data-toggle="tab"><i class="fa fa-envelope-o"></i>Inbox </a></li>
                <li><a href="#sent" data-toggle="tab"><i class="fa fa-reply-all"></i> Sent</a></li>
                <li><button class="btn btn-danger pull-right" id="signupbtn" data-toggle="modal" data-target="#myModalcompose">
                        Compose new
                    </button></li>
            </ul>

            <div class="tab-content">



                <div class="tab-pane active" id="inbox">

                    <?php foreach ($inbox as $inboxRow) { ?>
                        <div class="btn-toolbar well well-sm" role="toolbar"  style="margin:0px;">
                            <a type="button" data-toggle="collapse" data-target="#inbx<?php echo $inboxRow['id']; ?>">

                                <div class="btn-group col-md-3"><?php echo $inboxRow['FirstName'] . ' ' . $inboxRow['LastName']; ?></div>
                                <div class="btn-group col-md-8"><b><?php echo $inboxRow['msg_header']; ?></b>
                            </a>

                            <div class="pull-right">
                                <i class="glyphicon glyphicon-time"></i><?php echo $inboxRow['date']; ?> 
                                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-share-square-o"> Reply</i></button>
                            </div></div>
                    </div> 
                    <div id="inbx<?php echo $inboxRow['id']; ?>" class="collapse out well"><p><?php echo $inboxRow['msg_content']; ?></p>
                    </div> 


                <?php } ?>

            </div>

            <div class="tab-pane" id="sent">

                <?php foreach ($sent as $sentRow) { ?>
                    <div class="btn-toolbar well well-sm" role="toolbar"  style="margin:0px;">
                        <a type="button" data-toggle="collapse" data-target="#snt<?php echo $sentRow['id']; ?>">

                            <div class="btn-group col-md-3"><?php echo $sentRow['FirstName'] . ' ' . $sentRow['LastName']; ?></div>
                            <div class="btn-group col-md-8"><b><?php echo $sentRow['msg_header']; ?></b>
                        </a>

                        <div class="pull-right">
                            <i class="glyphicon glyphicon-time"></i><?php echo $sentRow['date']; ?> 

                        </div></div>
                </div> 
                <div id="snt<?php echo $sentRow['id']; ?>" class="collapse out well"><p><?php echo $sentRow['msg_content']; ?></p>
                </div> 


            <?php } ?>

        </div>
    </div>

</div>
</div>


</div>



<div class="container text-center">
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">

            <div class="modal-content"><br/><br/>


                <div class="modal-body">
                    <form class="form-horizontal">
                        <fieldset>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="body">Body :</label>  
                                <div class="col-md-8">
                                    <input id="body" name="body" type="text" placeholder="Message Body" class="form-control input-md">

                                </div>
                            </div>

                            <!-- Textarea -->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="message">Message :</label>
                                <div class="col-md-8">                     
                                    <textarea class="form-control" id="message" name="message"></textarea>
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="send"></label>
                                <div class="col-md-8">
                                    <button id="send" name="send" class="btn btn-primary">Send</button>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>

            </div>
        </div>




    </div>

</div>
<div class="container text-center">
    <!-- Button trigger modal -->
    <br>

    <!-- Modal -->
    <div class="modal fade" id="myModalcompose" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Compose new message</h4>
                </div>
                <div class="modal-body">
                    <?php echo form_open('welcome/new_message/1'); ?>
                    <fieldset>
                        <div class="row">

                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon">First Name:</span>
                                    <?php
                                    $data = array(
                                        'name' => 'firstname',
                                        'id' => 'firstname',
                                        'placeholder' => '',
                                        'class' => 'form-control',
                                        'maxlength' => "140",
                                    );
                                    echo form_input($data);
                                    ?>
                                </div>
                                <br>
                                
                                <div class="input-group">
                                    <span class="input-group-addon">Last Name:</span>
                                    <?php
                                    $data2 = array(
                                        'name' => 'lastname',
                                        'id' => 'lastname',
                                        'placeholder' => '',
                                        'class' => 'form-control',
                                        'maxlength' => "140",
                                    );
                                    echo form_input($data2);
                                    ?> 

                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="form-group">
                            <?php
                            $data = array(
                                'name' => 'msg_header',
                                'id' => 'msg_header',
                                'placeholder' => 'Subject',
                                'class' => 'form-control',
                                'maxlength' => "400",
                            );
                            echo form_input($data);
                            ?> 

                        </div>
                        <div class="form-group">
                            <?php
                            $data = array(
                                'name' => 'content',
                                'id' => 'content',
                                'placeholder' => 'Write your message:',
                                'class' => 'form-control',
                                'rows' => '6',
                                'maxlength' => "400",
                            );
                            echo form_textarea($data);
                            ?> 

                        </div>
                        <input class="btn btn-lg btn-success vertical-offset-100" type="submit" value="Send!">
                    </fieldset>
                    <?php echo form_close(); ?>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
</div>