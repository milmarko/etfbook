<div class="container">
    <div class="row">

        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="text-center">Settings and preferences</h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <fieldset>
                            <h4>Privacy</h4>

                            <div class="row">

                                <div class="col-md-6">
                                    <h5>Who can see my statuses?</h5>

                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioStatus" id="privatestatus" value="privatestatus" 
                                            <?php
                                            if ($status == false) {
                                                echo 'checked';
                                            }
                                            ?>> 
                                            Only friends


                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioStatus" id="publicstatus" value="publicstatus"
                                            <?php
                                            if ($status == true) {
                                                echo 'checked';
                                            }
                                            ?>>
                                            Everyone
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <h5>Who can see my photos?</h5>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioPhotos" id="privatephoto" value="privatephoto" 
                                            <?php
                                            if ($photos == false) {
                                                echo 'checked';
                                            }
                                            ?>>
                                            Only friends
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioPhotos" id="publicphoto" value="publicphoto"
                                            <?php
                                            if ($photos == true) {
                                                echo 'checked';
                                            }
                                            ?>>
                                            Everyone
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Button trigger modal -->
                                    <br>
                                    <button class="btn btn-info" data-toggle="modal" data-target="#myModalName">
                                        Change name
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="myModalName" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" id="submitbtn" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">Change your name</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <?php echo form_open('welcome/name_change_validation');
                                                    echo validation_errors();
                                                    ?>

                                                    <fieldset>
                                                        <div class="form-group">
                                                            <?php
                                                            $data = array(
                                                                'name' => 'firstname',
                                                                'id' => 'firstname',
                                                                'placeholder' => 'First name',
                                                                'class' => 'form-control',
                                                            );
                                                            echo form_input($data);
                                                            ?>

                                                        </div>
                                                        <div class="form-group">
                                                            <?php
                                                            $data = array(
                                                                'name' => 'lastname',
                                                                'id' => 'lastname',
                                                                'placeholder' => 'Last name',
                                                                'class' => 'form-control',
                                                            );
                                                            echo form_input($data);
                                                            ?>                  
                                                        </div>

                                                        <br><br>
                                                        <input class="btn btn-lg btn-success btn-block" type="submit" value="Save change">
                                                    </fieldset>

<?php echo form_close(); ?>
                                                </div>
                                                <div class="modal-footer">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- Button trigger modal -->
                                        <br>
                                        <button class="btn btn-info" data-toggle="modal" data-target="#myModalPass">
                                            Change password
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="myModalPass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" id="submitbtn" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title" id="myModalLabel">Change your password</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <?php echo form_open('welcome/password_change_validation');
                                                        echo validation_errors();
                                                        ?>

                                                        <fieldset>
                                                            <div class="form-group">
                                                                <?php
                                                                $data = array(
                                                                    'name' => 'password',
                                                                    'id' => 'password',
                                                                    'placeholder' => 'Password',
                                                                    'class' => 'form-control',
                                                                );
                                                                echo form_password($data);
                                                                ?> 
                                                            </div>
                                                            <div class="form-group">
                                                                <?php
                                                                $data = array(
                                                                    'name' => 'cpassword',
                                                                    'id' => 'cpassword',
                                                                    'placeholder' => 'Confirm password',
                                                                    'class' => 'form-control',
                                                                );
                                                                echo form_password($data);
                                                                ?> 

                                                            </div>

                                                            <br><br>
                                                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Save change">
                                                        </fieldset>

<?php echo form_close(); ?>
                                                    </div>
                                                    <div class="modal-footer">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <hr/>
                                <?php if($type==0) { ?>
                                        <?php } else if ($type == 2) { ?>
                                <h4> &nbsp;&nbsp; Admin requests </h4>
                                        <?php } ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php if($type==0) { ?>
                                        <input type="submit" style="margin:10px;" value="Request admin status" class="btn btn-warning">
                                        <?php } else if ($type == 2) { 
                                                    foreach($admin_requests as $request)
                                                    {
                                                        echo "<h4>"."&nbsp;&nbsp;"
                                                                ."  <a href=". base_url() . "index.php/welcome/profile/"
                                                                .$request['id']
                                                                .">"
                                                                .$request['FirstName'] . " " 
                                                                .$request['LastName'] . "<a/>"
                                                                ."  <a href=". base_url() . "index.php/welcome/aprove_admin/" 
                                                                .$request['id'] . ">Approve</a> <a href=" . base_url() . "index.php/welcome/decline_admin/" 
                                                                .$request['id']. ">Decline</a>" . "<h4>";
                                                    }
                                        }
                                        ?>
                                    </div>
                                </div>

                                <br/><br/>
                                <hr/>
                                <p><button id="save" type="button" class="btn btn-primary btn-lg btn-block"> Save changes </button></p>

                        </fieldset>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="http://mymaplist.com/js/vendor/TweenLite.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(document).click(function(event) {
            if (event.target.id == "save") {
                window.location.href = "<?php echo site_url('welcome/change_settings/') ?>/" + $('input[name="radioStatus"]:checked').val() + "/" + $('input[name="radioPhotos"]:checked').val();
            }
        });
        $(document).mousemove(function(e) {
            TweenLite.to($('body'),
                    .5,
                    {css:
                                {
                                    'background-position': parseInt(event.pageX / 8) + "px " + parseInt(event.pageY / 12) + "px, " + parseInt(event.pageX / 15) + "px " + parseInt(event.pageY / 15) + "px, " + parseInt(event.pageX / 30) + "px " + parseInt(event.pageY / 30) + "px"
                                }
                    });
        });
    });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript">
    $('.btn.btn-warning').click(function() {
        var id = $(this).attr('id');
        var url = "<?php echo base_url() . "index.php/welcome/request_admin"; ?>";
        
        alert("Requested");
        $.ajax({
            'url': url,
            'type': 'POST',
            'data': {'idf': id},
            'success': function(data) {
                if (data != null) {
                    //location.reload(true);
                    var idd = 1;
                }
            }
        });
        // alert(id);
    });
</script>
