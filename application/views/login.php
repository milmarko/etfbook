<!--Autor: Jovan Stupar-->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ETFbook Login</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/additional.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/login.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
      
    <div class="container">
      <div class="row vertical-offset-70">
        <div class="col-md-4 col-md-offset-4">
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h3 class="">ETFbook</h3>
              <h4 class="">welcome</h4>
            </div>
            <div class="panel-body">
              
              <?php echo form_open('welcome/login_validation'); ?>
                  <fieldset>
                      <div class="form-group">
                          <?php
                            $data=array(
                              'name'=>'email',
                              'id'=>'email',
                              'placeholder'=>'Email',
                              'class'=>'form-control',
                            );
                            echo form_input($data);
                          ?>
                         
                      </div>
                      <div class="form-group">
                          <?php
                            $data=array(
                              'name'=>'password',
                              'id'=>'password',
                              'placeholder'=>'Password',
                              'class'=>'form-control',
                            );
                            echo form_password($data);
                          ?>
                      </div>
                         <input class="btn btn-lg btn-success btn-block" type="submit" value="Login">
                  </fieldset>
              <?php echo form_close(); ?>
                <br/>
                
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <?php echo form_open('welcome/guest_login'); ?>
                 <input class="btn  btn-info btn-block" type="submit" value="Login as guest">
              <?php echo form_close(); ?>
                        
                    </div>
                    
                </div>
              
            
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container text-center">
      <!-- Button trigger modal -->
      <br>
      <div class="row">
         
          <div class="col-md-4 col-md-offset-4">
             <button class="btn btn-primary btn-lg btn-block" id="signupbtn" data-toggle="modal" data-target="#myModal">
        Sign up!
        </button> 
            
          </div>
          
      </div>
      
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" id="submitbtn" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">Welcome new member!</h4>
            </div>
            <div class="modal-body">
              <?php echo form_open('welcome/signup_validation'); 
                echo validation_errors();?>
                
                <fieldset>
                  <div class="form-group">
                      <?php
                            $data=array(
                              'name'=>'firstname',
                              'id'=>'firstname',
                              'placeholder'=>'First name',
                              'class'=>'form-control',
                            );
                            echo form_input($data);
                          ?>
                   
                  </div>
                  <div class="form-group">
                        <?php
                            $data=array(
                              'name'=>'lastname',
                              'id'=>'lastname',
                              'placeholder'=>'Last name',
                              'class'=>'form-control',
                            );
                            echo form_input($data);
                          ?>                  
                  </div>
                  <div class="form-group">
                      <?php
                            $data=array(
                              'name'=>'email',
                              'id'=>'email',
                              'placeholder'=>'e-mail',
                              'class'=>'form-control',
                            );
                            echo form_input($data);
                          ?> 
                  </div>
                  <div class="form-group">
                      <?php
                            $data=array(
                              'name'=>'password',
                              'id'=>'password',
                              'placeholder'=>'Password',
                              'class'=>'form-control',
                            );
                            echo form_password($data);
                          ?> 
                  </div>
                  <div class="form-group">
                       <?php
                            $data=array(
                              'name'=>'cpassword',
                              'id'=>'cpassword',
                              'placeholder'=>'Confirm password',
                              'class'=>'form-control',
                            );
                            echo form_password($data);
                          ?> 
            
                  </div>
                  <br><br>
                  <input class="btn btn-lg btn-success btn-block" type="submit" value="Sign Up">
                </fieldset>
              
              <?php echo form_close(); ?>
            </div>
            <div class="modal-footer">
              
            </div>
          </div>
        </div>
      </div>
    </div>




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="http://mymaplist.com/js/vendor/TweenLite.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
      
      $(document).mousemove(function(e){
      TweenLite.to($('body'), 
        .5, 
        { css: 
            {
                'background-position':parseInt(event.pageX/8) + "px "+parseInt(event.pageY/12)+"px, "+parseInt(event.pageX/15)+"px "+parseInt(event.pageY/15)+"px, "+parseInt(event.pageX/30)+"px "+parseInt(event.pageY/30)+"px"
            }
        });
  });
});
    </script>
  </body>
</html>