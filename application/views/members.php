

<div class="container"> 
    <div class="row">
        <div class="col-lg-8 ">
            <?php if($num_of_follow != 0) { ?> <h2 class="text-center">Recent status updates</h2><hr/>  <?php } ?>
            <?php if($num_of_follow == 0){ ?> <h2 class="text-center">Here are few ETFbook users you can follow </h2><hr/>  
                
             <ul class="list-unstyled ">
                <?php
                foreach ($random_users as $row) { ?>
                        <li id="<?php echo $row['id'] ?>"class="well">
                            <article>
                                <div class="media">
                                    <a href="<?php echo base_url() . 'index.php/welcome/profile/' . $row['id'] ?>" class="pull-left text-center"><img src="<?php echo $row['picture'] ?>" class="img-circle media-object" width="100" height="100"><h5><?php echo $row['FirstName'] . ' ' . $row['LastName']; ?></h5></a>
                                    <div class="media-body">
                                            <div class="col-lg-4">
                                                <ul class="list-unstyled pull-right">
                                                    <li class="pull-right">
                                                        <p><button id="<?php echo $row['id'] ?>" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Follow</button></p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>                  
                                    </div>
                            </article>
                        </li>
                <?php 
}
?>
            </ul>    
                
                <?php } ?>
            <ul class="list-unstyled ">
                <?php
                foreach ($data as $row) {
                    if ($row['URL'] == NULL && ($row['publicstatus'] || $row['userid'] == $this->session->userdata('id'))) {
                        ?>
                        <li id="<?php echo $row['id'] ?>"class="well">
                            <article>
                                <div class="media">
                                    <a href="<?php echo base_url() . 'index.php/welcome/profile/' . $row['userid'] ?>" class="pull-left text-center"><img src="<?php echo $row['picture'] ?>" class="img-circle media-object" width="100" height="100"><h5><?php echo $row['FirstName'] . ' ' . $row['LastName']; ?></h5></a>
                                    <div class="media-body">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <h3 class="media-heading"><?php $this->load->model('status');
                        echo $this->status->replace_hashtags($row['content']); ?></h3>
                                                <ul class="list-inline">
                                                    <li>
                                                        <span  class="numlikes glyphicon glyphicon-time text-muted"><?php echo ' ' . $row['date']; ?></span>
                                                    </li>
                                                    <li>
                                                        <span id="numlikes" class="glyphicon glyphicon-hand-up text-muted"><?php echo $row['numlikes']; ?></span>
                                                    </li>
                                                </ul>
                                                <br>

                                            </div>
                                            <div class="col-lg-4">
                                                <ul class="list-unstyled pull-right">
                                                    <li class="pull-right">
                                                        <p><button id="<?php echo $row['id'] ?>" type="button" class="btn btn-info <?php if (in_array($this->session->userdata('id'), $row['supports']) != false) echo "disabled"; ?>"><span class="glyphicon glyphicon-hand-up"></span> Support</button></p>
                                                    </li>
                                                    <?php if($row['userid'] != $this->session->userdata('id')){ ?>
                                                    <li class="pull-right">
                                                        <p><button type="button" class="btn btn-warning send_msg" data-toggle="modal" data-target="#myModalcompose"  data-id="<?php echo $row['userid']; ?>" data-fname="<?php echo $row['FirstName']; ?>" data-lname="<?php echo $row['LastName']; ?>"><span class="glyphicon glyphicon-envelope" data-toggle="modal" data-target="#myModalcompose" ></span>Send message</button></p>
                                                    </li>
                                                    <?php } ?>
                                                    
                                                    <?php if($type == 2) { ?>
                                                    <li class="pull-right">
                                                        <p><button id="<?php echo $row['id'] ?>" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Remove</button></p>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>                  
                                    </div>
                                </div>
                            </article>
                        </li>
                <?php }
}
?>
            </ul>
        </div>
         <?php if($num_of_follow != 0) { ?>
        <div class="col-lg-4">
            <h2 class="text-center">Recent image updates</h2><hr/>
            <ul class="list-unstyled">
            <?php foreach ($data as $row) {
                if ($row['URL'] != NULL && ($row['publicphotos'] || $row['userid'] == $this->session->userdata('id'))) { ?>
                    <li id="<?php echo $row['id'] ?>"class="well">
                        <div class="media">
                            <div class="thumbnail">
                                <img src="<?php echo $row['URL']; ?>">
                                <div class="caption">
                                    <div>
                                        <img src="<?php echo $row['picture'] ?>" class="img-circle pull-right" width="50" height="50">
                                        <p class="pull-right"><a href="<?php echo base_url() . 'index.php/welcome/profile/' . $row['userid'] ?>"><?php echo $row['FirstName'] . ' ' . $row['LastName']; ?></a></p>
                                    </div>

                                    <h3><?php $this->load->model('status');
                        echo $this->status->replace_hashtags($row['content']); ?></h3>
                                    <ul class="list-inline">
                                        <li>
                                            <span  class="numlikes glyphicon glyphicon-time text-muted"><?php echo ' ' . $row['date']; ?></span>
                                        </li>
                                        <li>
                                            <span id="numlikes" class="glyphicon glyphicon-hand-up text-muted"><?php echo $row['numlikes']; ?></span>
                                        </li>
                                    </ul>
                                    <br>
                                    <p>
                                        <button id="<?php echo $row['id'] ?>" type="button" class="btn btn-info <?php if (in_array($this->session->userdata('id'), $row['supports']) != false) echo "disabled"; ?>"><span class="glyphicon glyphicon-hand-up"></span> Support</button>
                                        <?php if($row['userid'] != $this->session->userdata('id')){ ?>
                                        <button type="button" class="btn btn-warning send_msg" data-toggle="modal" data-target="#myModalcompose"  data-id="<?php echo $row['userid']; ?>" data-fname="<?php echo $row['FirstName']; ?>" data-lname="<?php echo $row['LastName']; ?>"><span class="glyphicon glyphicon-envelope" data-toggle="modal" data-target="#myModalcompose" ></span>Send message</button>
                                        <?php } ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php }
            } ?>
            </ul>
        </div>
         <?php } ?>
    </div>
</div>

<div class="stupar">

</div>

<!-- Ovo je modal za slanje poruke -->
<div class="modal fade" id="myModalcompose" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Compose new message</h4>
            </div>
            <div class="modal-body send_msg_modal">
<?php echo form_open('welcome/new_message_from_members'); ?>
                <fieldset>
                    <div class="row">

                        <div class="col-lg-8">
                            <div class="input-group">
                                <span class="input-group-addon ">First Name:</span>
                                <?php
                                $data2 = array(
                                    'name' => 'firstname',
                                    'id' => 'firstname',
                                    'placeholder' => 'f',
                                    'class' => 'form-control',
                                    'maxlength' => "140",
                                );
                                echo form_input($data2);
                                ?>   

                            </div>

                            <br>

                            <div class="input-group">
                                <span class="input-group-addon">Last Name:</span>
                                <?php
                                $data2 = array(
                                    'name' => 'lastname',
                                    'id' => 'lastname',
                                    'placeholder' => 'l',
                                    'class' => 'form-control',
                                    'maxlength' => "140",
                                );
                                echo form_input($data2);
                                ?> 

                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group">
                        <?php
                        $data = array(
                            'name' => 'msg_header',
                            'id' => 'msg_header',
                            'placeholder' => 'Subject',
                            'class' => 'form-control',
                            'maxlength' => "400",
                        );
                        echo form_input($data);
                        ?> 

                    </div>
                    <div class="form-group">
                        <?php
                        $data = array(
                            'name' => 'content',
                            'id' => 'content',
                            'placeholder' => 'Write your message:',
                            'class' => 'form-control',
                            'rows' => '6',
                            'maxlength' => "400",
                        );
                        echo form_textarea($data);
                        ?> 

                    </div>
                    <input class="btn btn-lg btn-success vertical-offset-100" type="submit" value="Send!">
                </fieldset>
<?php echo form_close(); ?>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript">
    $('.btn.btn-info').click(function() {
        var id = $(this).attr('id');
        var elem = $(this).closest(".media").find('#numlikes').text();
        $(this).addClass('disabled');
        elem++;
        elem = $(this).closest(".media").find('#numlikes').text(elem);
        $.ajax({
            'url': "<?php echo base_url() . 'index.php/welcome/support/' ?>",
            'type': 'POST',
            'data': {'id': id},
            'success': function(msg) {
                //$('.stupar').html(msg);
                var idd = 1;
            }
        });
    });


    $(document).on("click", ".send_msg", function() {
        $(".modal-body #firstname").val($(this).data('fname'));


        $(".modal-body #lastname").val($(this).data('lname'));

    });


</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript">
    $('.btn.btn-danger').click(function() {
        var id = $(this).attr('id');
        
        $.ajax({
            'url': "<?php echo base_url() . 'index.php/welcome/remove_status/' ?>",
            'type': 'POST',
            'data': {'id': id},
            'success': function(msg) {
                location.reload(true);
                var idd = 1;
            }
        });
    });


    $(document).on("click", ".send_msg", function() {
        $(".modal-body #firstname").val($(this).data('fname'));


        $(".modal-body #lastname").val($(this).data('lname'));

    });


</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript">
    $('.btn.btn-primary').click(function() {
        var id = $(this).attr('id');
        $(this).addClass('disabled');
        var url = "<?php echo base_url() . "index.php/welcome/new_follow/"; ?>";
        
        //alert(url);
        $.ajax({
            'url': url,
            'type': 'POST',
            'data': {'idf': id},
            'success': function(data) {
                if (data != null) {
                    var idd = 1;
                }
            }
        });
        // alert(id);
    });
</script>