<?php

class User extends CI_Model{
    public function can_log_in(){
        $this->db->where('email',$this->input->post('email'));
        $this->db->where('password',md5($this->input->post('password')));
        $query=$this->db->get('user');
        
        if($query->num_rows()==1){
            return true;
        }else{
            return false;
        }        
    }
    
    public function add_user(){
        $query=$this->db->get('user');
        $count = $query->num_rows(); 
        
        $type = 0;
        if($count == 0) {
            $type = 2;
        }
        
        $data=array(
            'email'=>$this->input->post('email'),
            'password'=>md5($this->input->post('password')),
            'FirstName'=>$this->input->post('firstname'),
            'LastName'=>$this->input->post('lastname'),
            'picture'=>base_url()."assets/img/drazen.jpg",
            'cover'=>"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSObTXtkAERFReDYrm8YuZ4JUHfiBT1C04LRMzLv2C_qL5HIwIgcw",
            'type'=>$type
        );      
        
        $ret=$this->db->insert('user',$data);
        
        if(!$ret){
            echo "FATAL ERROR";
        }
    }
    
    public function getID(){
        $ret=$this->db->get_where('user',array('email'=>$this->input->post('email')));
       
        foreach($ret->result() as $row){
            return $row->id; 
        }
    }
    
    public function get_current_user(){
        $ret=$this->db->get_where('user',array('email'=>$this->input->post('email')));
        
        $user=array();
        foreach($ret->result() as $row){
            $user['id']=$row->id;
            $user['FirstName']=$row->FirstName;
            $user['LastName']=$row->LastName;
            $user['picture']=$row->picture;
        }
        return $user;
    }
    
    public function get_this_user(){
        $userid = $this->session->userdata('id');
        $results = $this->db->get_where('user', array('id'=>$userid));
        //$request_admin = 1;
        //$users_admin_requests = $this->db->get_where('user', array('type'=>$request_admin));
        
        $temp = array();
        
        foreach($results->result() as $user) {
            //There will be only one
            $temp['id'] = $user->id;
            $temp['FirstName']=$user->FirstName;
            $temp['LastName']=$user->LastName;
            $temp['picture']=$user->picture;
            $temp['publicphotos']=$user->publicphotos;
            $temp['publicstatus']=$user->publicstatus;
            $temp['cover']=$user->cover;
            $temp['type']=$user->type;
            //$temp['admin_requests']=$users_admin_requests;
             
            return $temp;
        }       
    }
    
    public function get_admin_requests(){
        $request_admin = 1;
        $ret = $this->db->get_where('user', array('type'=>$request_admin));
        
        $data=array();        
        foreach ($ret->result() as $row){
            $user=array();
            
            $user['id']=$row->id;
            $user['type']=$row->type;
            $user['FirstName']=$row->FirstName;
            $user['LastName']=$row->LastName;
           
            $data[]=$user;              
        }     
        return $data;
    }
    
    public function get_user($id) {
        $results = $this->db->get_where('user', array('id'=>$id));
        $temp = array();
        
        foreach($results->result() as $user) {
            //There will be only one
            $temp['id'] = $user->id;
            $temp['FirstName']=$user->FirstName;
            $temp['LastName']=$user->LastName;
            $temp['picture']=$user->picture;
            $temp['cover']=$user->cover;
            $temp['publicphotos'] = $user->publicphotos;
            $temp['publicstatus'] = $user->publicstatus;
            return $temp;
        }       
    }
    
    public function add_follow($follower, $followee){
        $data = array(
          'fk_follower' => $follower,
          'fk_followee' => $followee
        );
        $ret = $this->db->insert('follow', $data);
    }
    
     public function remove_follow($follower, $followee){
        $data = array(
          'fk_follower' => $follower,
          'fk_followee' => $followee
        );
        $ret = $this->db->delete('follow', $data);
    }
    
    public function get_followers($id){
        
        $ret=$this->db->get_where('follow',array('fk_followee'=>$id));
        
        $data=array();
        foreach($ret->result() as $row){
            $data[]=$row->fk_follower;
        }
        return $data;
    }
    
    public function request_admin_status(){
        $type = 1;
        $id = $this->session->userdata('id');
        $this->db->set('type', $type);
        $this->db->where('id', $id);
        $this->db->update('user');       
    }
    
    public function decline_admin($id) {
        $type = 0;
        $this->db->set('type', $type);
        $this->db->where('id', $id);
        $this->db->update('user'); 
    }
    
    public function aprove_admin($id) {
        $type = 2;
        $this->db->set('type', $type);
        $this->db->where('id', $id);
        $this->db->update('user'); 
    }
    
    public function get_type() {
        $userid = $this->session->userdata('id');
        $results = $this->db->get_where('user', array('id'=>$userid));
       
        foreach($results->result() as $user) {
            return $user->type;
        }      
    }
    
    public function num_following(){
        $userid = $this->session->userdata('id');
        $results = $this->db->get_where('follow', array('fk_follower'=>$userid));
        
        return $results->num_rows();
    }  
    
    public function random_users(){
        $this->db->order_by('id', 'RANDOM');
        $this->db->limit(5);
        $this->db->where('id !=', $this->session->userdata('id'));
        $query = $this->db->get('user');
        
        $data=array();        
        foreach ($query->result() as $row){
            $user=array();
            
            $user['id']=$row->id;
            $user['FirstName']=$row->FirstName;
            $user['LastName']=$row->LastName;
            $user['picture']=$row->picture;
           
            $data[]=$user;              
        }     
        return $data;
    }

    public function list_all_users(){
         $query=$this->db->get('user');

         $data=array();
         foreach($query->result() as $row){
            $user['id']=$row->id;
            $user['FirstName']=$row->FirstName;
            $user['LastName']=$row->LastName;
            $user['picture']=$row->picture;
           
            $data[]=$user;    
         }
         return $data;
    }
}

