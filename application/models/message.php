<?php


class Message extends CI_Model{
   
    public function get_all_inbox(){
       $currentID=$this->session->userdata('id');
       
       $query=$this->db->get_where('message',array('fk_user_to'=>$currentID));
       
       $retQ=array();
       
       foreach($query->result() as $row){
           $temp=array();
           
           $temp['id']=$row->id;
           $temp['msg_header']=$row->msg_header;
           $temp['msg_content']=$row->msg_content;
           $temp['date']=  unix_to_human($row->unixdate,false,'eu');
           
           $fromID=$row->fk_user_from;
           
           $userData=$this->db->get_where('user',array('id'=>$fromID));
           foreach($userData->result() as $user){
               //There will be only one, we still don't know how to do this without the loop :)
               $temp['FirstName']=$user->FirstName;
               $temp['LastName']=$user->LastName;
               $temp['picture']=$user->picture;
           }
           
           $retQ[]=$temp;
       }
       
       return $retQ;
    }
    
    public function get_all_sent(){
       $currentID=$this->session->userdata('id');
       
       $query=$this->db->get_where('message',array('fk_user_from'=>$currentID));
       
       $retQ=array();
       
       foreach($query->result() as $row){
           $temp=array();
           
           $temp['id']=$row->id;
           $temp['msg_header']=$row->msg_header;
           $temp['msg_content']=$row->msg_content;
           $temp['date']=  unix_to_human($row->unixdate,false,'eu');
           
           $toID=$row->fk_user_to;
           
           $userData=$this->db->get_where('user',array('id'=>$toID));
           foreach($userData->result() as $user){
               //There will be only one, we still don't know how to do this without the loop :)
               $temp['FirstName']=$user->FirstName;
               $temp['LastName']=$user->LastName;
               $temp['picture']=$user->picture;
           }
           
           $retQ[]=$temp;
       }
       
       return $retQ;
    }
    
    public function check_user($firstName,$lastName){
        $this->db->where('FirstName',$this->input->post('firstname'));
        $this->db->where('LastName',$this->input->post('lastname'));
        $query=$this->db->get('user');
        
        if($query->num_rows()==1){
            foreach($query->result() as $user){
                return $user->id;
            }
        }else{
            return NULL;
        }
    }
    
    public function insert_new($idTo){

         $data=array(
            'fk_user_from'=>$this->session->userdata('id'),
            'fk_user_to'=>$idTo,
            'msg_header'=>$this->input->post('msg_header'),
            'msg_content'=>$this->input->post('content'),
            'unixdate'=>now(),
        );
        
        $this->db->insert('message',$data);
    }
}
