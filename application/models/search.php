<?php

class Search extends CI_Model{
   function get_search($match) {
    $this->db->order_by('unixdate','desc');$this->db->select('*');
    $this->db->from('status');
    $this->db->join('hashtag', 'status.id = hashtag.fk_status');
    $this->db->having('tag', $match);
    $query = $this->db->get();

    $data= array();
    foreach ($query->result() as $row){
        $temp=array();

        $temp['content']=$row->content;
        $temp['numlikes']=$row->numlikes;
        $temp['id']=$row->id;
        $temp['URL'] = $row->URL;
        $date=$row->unixdate;

        $temp['date']=unix_to_human($date, false, 'eu');

        $userid=$row->fk_user;
        $userData=$this->db->get_where('user',array('id'=>$userid));

        foreach($userData->result() as $user){
            //There will be only one
            $temp['FirstName']=$user->FirstName;
            $temp['LastName']=$user->LastName;
            $temp['picture']=$user->picture;
            $temp['userid']=$user->id;
            $temp['publicphotos'] = $user->publicphotos;
            $temp['publicstatus'] = $user->publicstatus;
        }

        $supports=$this->db->get_where('supports',array('fk_status'=>$row->id));
        $temp2=array();
        foreach($supports->result() as $supp){
            $temp2[]=$supp->fk_user;
        }
        $temp['supports']=$temp2; //this works
        $data[$row->id]=$temp;

    }

    return $data;
  }

}