<?php

class Settings extends CI_Model {

    public function save_changes($selected_status,$selected_photos ){
        $userid = $this->session->userdata('id');
        if ($selected_status == 'privatestatus') {
            $publicstatus = false;  
        }
        else if ($selected_status == 'publicstatus') {
            $publicstatus = true;
        }
        if ($selected_photos == 'privatephoto') {
            $publicphotos = false;  
        }
        else if ($selected_photos == 'publicphoto') {
            $publicphotos = true;
        }
        $data = array(
                       'publicphotos' => $publicphotos,
                       'publicstatus' => $publicstatus,
                       
                    );

        $this->db->where('id', $userid);
        $this->db->update('user', $data); 
    }
    
    public function change_name(){
        $userid = $this->session->userdata('id');
        
        $data = array(
            'FirstName' => $this->input->post('firstname'),
            'LastName' => $this->input->post('lastname')
        );
        $this->db->where('id', $userid);
        $this->db->update('user', $data);
        
        $this->session->set_userdata('FirstName',$this->input->post('firstname'));
        
        $this->session->set_userdata('LastName',$this->input->post('lastname'));
    }
    public function change_password(){
        $userid = $this->session->userdata('id');
        $data = array(
            'password' => md5($this->input->post('password'))
        );
        $this->db->where('id', $userid);
        $this->db->update('user', $data);
    }
    public function change_photo($img){
        $userid = $this->session->userdata('id');
        $data = array(
            'picture' => $img
        );
        $this->db->where('id', $userid);
        $this->db->update('user', $data);
		$this->session->userdata('picture') = $img;
    }
    
    public function change_cover_photo($img){
        $userid = $this->session->userdata('id');
        $data = array(
            'cover' => $img
        );
        $this->db->where('id', $userid);
        $this->db->update('user', $data);
    }
}
