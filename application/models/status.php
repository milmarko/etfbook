<?php

class Status extends CI_Model{
   
    public function get_all(){
        /*$this->db->order_by('unixdate','desc');
        $this->db->select();
        $this->db->from('status');
        $this->db->join('follow', 'follow.fk_followee = status.fk_user');
        $this->db->where('status.fk_user', $this->session->userdata('id'));
        $this->db->or_where('fk_follower',$this->session->userdata('id'));
        
        $query=$this->db->get();
        */
        if($this->session->userdata('is_logged_in')){
        if($this->db->count_all('follow')>0){
        $query = $this->db->query("select distinct content, numlikes, id, URL, unixdate, fk_user from status, follow where status.fk_user = ".$this->session->userdata('id')." or (follow.fk_followee = status.fk_user and follow.fk_follower = ".$this->session->userdata('id').") order by unixdate desc");
        }
        else
        {
        $query = $this->db->query("select * from status where status.fk_user = ".$this->session->userdata('id'));
        }
        }
        else{
            $this->db->order_by('unixdate','desc');
            $query = $this->db->get('status');
        }
        $data=array();
        
        foreach ($query->result() as $row){
            $temp=array();
            
            $temp['content']=$row->content;
            $temp['numlikes']=$row->numlikes;
            $temp['id']=$row->id;
            $temp['URL'] = $row->URL;
            
            $date=$row->unixdate;            
            $temp['date']=unix_to_human($date, false, 'eu');
            
            $userid=$row->fk_user;
            $temp['userid']=$userid;
            $userData=$this->db->get_where('user',array('id'=>$userid));
            
            foreach($userData->result() as $user){
                //There will be only one
                $temp['FirstName']=$user->FirstName;
                $temp['LastName']=$user->LastName;
                $temp['picture']=$user->picture;
                $temp['picture']=$user->picture;
                $temp['publicstatus']=$user->publicstatus;
                $temp['publicphotos']=$user->publicphotos;
            }
            
            $supports=$this->db->get_where('supports',array('fk_status'=>$row->id));
            $temp2=array();
            foreach($supports->result() as $supp){
                $temp2[]=$supp->fk_user;
            }
            $temp['supports']=$temp2; //this works
            $data[]=$temp;  
            
        }
     
        return $data;
    }
    
    /*
    public function upload_new(){
        $data=array(
            'content'=>$this->input->post('status'),
            'fk_user'=>$this->session->userdata('id'),
            'unixdate'=>now(),
        );
        
        $this->db->insert('status',$data);
    }*/
    
    
     public function incrementLikes($id){
        $st=$this->db->get_where('status',array('id'=>$id));
        
        foreach($st->result() as $status){
            $numlikes=$status->numlikes+1;
            echo $numlikes;
            $this->db->where('id',$status->id);
            $this->db->update('status',array('numlikes'=>$numlikes));
        }
    }
    /**
     * Gets all status from user specified with $id
     * @param int $id
     */
    public function get_user_status($id){
       $this->db->order_by('unixdate','desc');
       $query=$this->db->get_where('status',array('fk_user'=>$id));

       $data= array();
       foreach ($query->result() as $row) {
           $temp = array();
           $temp['content']=$row->content;
           $temp['numlikes']=$row->numlikes;
           $temp['URL']=$row->URL;
           $temp['id']=$row->id;
           $date=$row->unixdate;            
           $temp['date']=unix_to_human($date, false, 'eu');
           $supports=$this->db->get_where('supports',array('fk_status'=>$row->id));
           $temp2 = array();
           foreach($supports->result() as $supp){
                $temp2[]=$supp->fk_user;
            }
           $temp['supports']=$temp2; //this works
           $data[$row->id]=$temp;
           $data[$row->id] = $temp;
       }
       return $data;
    }
    
    public function get_hashtag_status($string){
        $this->db->order_by('unixdate','desc');
        $this->db->select('*');
        $this->db->from('status');
        $this->db->join('hashtag', 'status.id = hashtag.fk_status');
        $this->db->having('tag', $string);
        $query = $this->db->get();
       
        $data= array();
        foreach ($query->result() as $row){
            $temp=array();
            
            $temp['content']=$row->content;
            $temp['numlikes']=$row->numlikes;
            $temp['id']=$row->id;
            $temp['URL'] = $row->URL;
            $date=$row->unixdate;
            
            $temp['date']=unix_to_human($date, false, 'eu');
            
            $userid=$row->fk_user;
            $userData=$this->db->get_where('user',array('id'=>$userid));
            
            foreach($userData->result() as $user){
                //There will be only one
                $temp['FirstName']=$user->FirstName;
                $temp['LastName']=$user->LastName;
                $temp['picture']=$user->picture;
                $temp['userid']=$user->id;
                $temp['publicphotos'] = $user->publicphotos;
                $temp['publicstatus'] = $user->publicstatus;
            }
            
            $supports=$this->db->get_where('supports',array('fk_status'=>$row->id));
            $temp2=array();
            foreach($supports->result() as $supp){
                $temp2[]=$supp->fk_user;
            }
            $temp['supports']=$temp2; //this works
            $data[$row->id]=$temp;
            
        }
     
        return $data;
       
    }
    
    public function replace_hashtags($string) {
	 preg_match_all('/#(\w+)/',$string,$matches);
	  foreach ($matches[1] as $match) {
              //if($this->session->userdata('is_logged_in'))
                $string = str_replace("#$match", "<a href='".base_url()."index.php/welcome/hashtag/$match'>#$match</a>", "$string");
              //else
                // $string = str_replace("#$match", "<a href='".base_url()."index.php/welcome/guest_hashtags/$match'>#$match</a>", "$string"); 
	  }
	 return $string;
	}
    
    public function upload_new($img){
        $data=array(
            'content'=>$this->input->post('status'),
            'fk_user'=>$this->session->userdata('id'),
            'unixdate'=>now(),
            'URL'=>$img
        );
        $this->db->insert('status',$data);
        $id = $this->db->insert_id();
        $hashtags = $this->status->get_hashtags($this->input->post('status'));
        foreach($hashtags as $hashtag){
            $hash_data = array(
              'tag'=>$hashtag,
              'fk_status' =>$id
            );
            $query = $this->db->get_where('hashtag', $hash_data);
            if($query->num_rows()==0){
                $this->db->insert('hashtag', $hash_data);
            }
        }
        
    } 
    
   
    public function get_hashtags($string) {
	 preg_match_all('/#(\w+)/',$string,$matches);
	  /*$i = 0;
	  if ($str) {
	   foreach ($matches[1] as $match) {
	   $count = count($matches[1]);
	   $keywords .= "$match";
	   $i++;
	   if ($count > $i) $keywords .= ", ";
	  }
	 } else {*/
         $keyword = [];
	   foreach ($matches[1] as $match) {
	    $keyword[] = $match;
            
	   }
	  $keywords = $keyword;
	//}
          
	return $keywords;
	}
        
        public function remove($id) {
            $this->db->delete('status', array('id' => $id)); 
        }

}


