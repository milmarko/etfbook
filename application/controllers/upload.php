<?php

class Upload extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}

	function index()
	{
		$this->load->view('upload_form', array('error' => ' ' ));
	}

	function do_upload()
	{
		$config['upload_path'] = './assets/img';
		$config['allowed_types'] = 'gif|jpg|png';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('upload_form', $error);
                   
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
                        $dat = $data['upload_data'];
                        $img = base_url().'assets/img/'.$dat['file_name'];
                        $this->load->model('settings');
                        $this->settings->change_photo($img);
                        redirect('welcome/my_profile');
		}
	}
        
        function do_upload_status_photo()
	{
		$config['upload_path'] = './assets/img';
		$config['allowed_types'] = 'gif|jpg|png';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('upload_form', $error);
                   
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
                        $dat = $data['upload_data'];
                        $img = base_url().'assets/img/'.$dat['file_name'];
                        $this->load->model('status');
                        //$this->settings->change_photo($img);
                        redirect('welcome/my_profile');
		}
	}
        
        function do_upload_cover()
	{
		$config['upload_path'] = './assets/img';
		$config['allowed_types'] = 'gif|jpg|png';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('upload_form', $error);
                   
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
                        $dat = $data['upload_data'];
                        $img = base_url().'assets/img/'.$dat['file_name'];
                        $this->load->model('settings');
                        $this->settings->change_cover_photo($img);
                        redirect('welcome/my_profile');
		}
	}
}
?>