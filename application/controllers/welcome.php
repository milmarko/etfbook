<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('login');
                
	}
        
        public function members(){            
            if($this->session->userdata('is_logged_in')){
                $this->load->view('header');
                
                $this->load->model('status');
                $this->load->model('user');
                
                $data=$this->status->get_all();
                
                $type=$this->user->get_type(); 
                $num_of_follow=$this->user->num_following();
                
                $random_users=$this->user->random_users();
                
                $this->load->view('members',array('data'=>$data, 'type'=>$type, 'num_of_follow'=>$num_of_follow, 'random_users'=>$random_users));
                
                $this->load->view('footer');
            }else{
                redirect('welcome/guest_login');
            }
        }
     
        
        
       public function new_status_(){
            $this->form_validation->set_rules('status','status','required|trim|xss_clean');
            
            if($this->form_validation->run()){
                $this->load->model('status');
                $this->status->upload_new();
                 redirect('welcome/members');
            }else{
                redirect('welcome/members');
            }
        }
        
        public function restricted(){
            echo '<h1>You dont have access</h1>';
        }
        
        public  function login_validation(){
          
            
            $this->form_validation->set_rules('email','Email','required|trim|xss_clean|callback_validate_email');
            $this->form_validation->set_rules('password','Password','required|md5|trim');
            
            if($this->form_validation->run()){
                $this->load->model('user');
                $user=$this->user->get_current_user();
                $data=array(
                    'id'=>$user['id'],
                    'FirstName'=>$user['FirstName'],
                    'LastName'=>$user['LastName'],
                    'picture'=>$user['picture'],
                    'email'=>$this->input->post('email'),
                    'is_logged_in'=>1
                );
                $this->session->set_userdata($data);
                
                redirect('welcome/members');
            }else{
                $this->load->view('login');
            }
        }
        
        public function validate_email(){
            $this->load->model('user');
            
            if($this->user->can_log_in()){
                return true;
            }else{
                $this->form_validation->set_message('validate_email','Incorrect input');
                return false;
            }
        }
        
        public function logout(){
            $this->session->sess_destroy();
            redirect('welcome/index');
        }
        
        public function signup_validation(){
           $this->form_validation->set_rules('firstname','First Name','trim|required|max_length[30]|xss_clean');
           
           $this->form_validation->set_rules('lastname','Last Name','trim|required|max_length[30]|xss_clean');
           
           $this->form_validation->set_rules('email','Email','required|trim|xss_clean|valid_email|is_unique[user.email]');
           
           $this->form_validation->set_rules('password','Password','required|trim|xss_clean');
           
           $this->form_validation->set_rules('cpassword','Confirm Password','required|trim|xss_clean|matched[password]');
           
           $this->form_validation->set_message('is_unique',"That email address already exists.");
           
           if($this->form_validation->run()){
               $this->load->model('user');
               $this->user->add_user(); 
               
               $user=$this->user->get_current_user();
                $data=array(
                    'id'=>$user['id'],
                    'FirstName'=>$user['FirstName'],
                    'LastName'=>$user['LastName'],
                    'picture'=>$user['picture'],                   
                    'email'=>$this->input->post('email'),
                    'is_logged_in'=>1
                );
                $this->session->set_userdata($data);
                
                redirect('welcome/members');               
               
           }else{
               $this->load->view('login');
               
           }
        }
        
        public function tempHome(){
            $this->load->view('header.php');
        }
        
        /**
         * TODO: Implement Home 
         */
        public function home(){
            redirect('welcome/members');
        }
        
        public function my_profile(){
            $this->load->view('header');
            $this->load->model('user');
            $this->load->model('status');
            $user_data = $this->user->get_this_user();
            $user_status = $this->status->get_user_status($user_data['id']);
            //print_r($user_status);
            $data = array(
                'user' => $user_data,
                'status' => $user_status,
                'my_id' => $this->session->userdata('id'),
            );
            $this->load->view('profile', $data);
            $this->load->view('footer');
        }
        
        public function profile($id){
            $this->load->view('header');
            $this->load->model('user');
            $this->load->model('status');
            $user_data = $this->user->get_user($id);
            $user_status = $this->status->get_user_status($user_data['id']);
            
            $followers=$this->user->get_followers($id);
            
            $data = array(
                'user' => $user_data,
                'status' => $user_status,
                'my_id' => $this->session->userdata('id'),
                'followers'=>$followers
            );
            $this->load->view('profile', $data);
            $this->load->view('footer');
        }
        
        public function hashtag($string){
                $this->load->view('header');
                
                $this->load->model('status');
                
                $data=$this->status->get_hashtag_status($string);
                if($this->session->userdata('is_logged_in'))
                    $this->load->view('hashtag',array('data'=>$data, 'my_id' => $this->session->userdata('id'), 'tag'=>$string));
                else
                    $this->load->view('guest_hashtags',array('data'=>$data, 'my_id' => $this->session->userdata('id'), 'tag'=>$string));
                $this->load->view('footer');

        }
        
        /**
         * TODO: Implement Messages
         */
        public function messages(){
            if($this->session->userdata('is_logged_in')){
                              
                $this->load->model('message');
                
                $inbox=$this->message->get_all_inbox();
                
                $sent=$this->message->get_all_sent();
            
                $this->load->view('header');
				$this->load->model('user');
                $user_data = $this->user->get_this_user();
            
                $this->load->view('messages',array('inbox'=>$inbox,'sent'=>$sent, 'cover'=>$user_data['cover']));
                
                $this->load->view('footer');
            }else{
                redirect('welcome/restricted');
            }
        }
        
        public function support(){
            $this->load->model('status');
            
            $id=$this->input->post('id');
            $this->status->incrementLikes($id);
            
            
            $this->load->model('supports');
            $this->supports->user_liked($id);
            
            $data=$this->status->get_all();
                //$this->load->view('members',array('data'=>$data));

        }
        
        public function support2(){
            $this->load->model('status');
            
            $id='8';
            $this->status->incrementLikes($id);
            
            
            $this->load->model('supports');
            $this->supports->user_liked($id);
            
            $data=$this->status->get_all();
                //$this->load->view('members',array('data'=>$data));
        }
        
        public function remove_status(){
            $this->load->model('status');
            
            $id=$this->input->post('id');
            $this->status->remove($id);
        }
        
        public function new_message(){
            $this->form_validation->set_rules('firstname','First Name','trim|required|max_length[30]|xss_clean');
            $this->form_validation->set_rules('lastname','Last Name','trim|required|max_length[30]|xss_clean');
            $this->form_validation->set_rules('content','content','trim|required|xss_clean');
            $this->form_validation->set_rules('msg_header','Subject','trim|required|xss_clean');
    
                   
            if($this->form_validation->run()){
                $firstName=$this->input->post('firstname');
                $lastName=$this->input->post('lastname');
                
                $this->load->model('message');
                $userTo=$this->message->check_user($firstName,$lastName);
                if($userTo==NULL){
                    redirect('welcome/messages'); 
                }else{

                    $this->message->insert_new($userTo);
                    redirect('welcome/messages');
                }
                
                
            }else{
               redirect('welcome/messages');
            }
        }
        
        public function new_message_from_members(){
            $this->form_validation->set_rules('firstname','First Name','trim|required|max_length[30]|xss_clean');
            $this->form_validation->set_rules('lastname','Last Name','trim|required|max_length[30]|xss_clean');
            $this->form_validation->set_rules('content','content','trim|required|xss_clean');
            $this->form_validation->set_rules('msg_header','Subject','trim|required|xss_clean');
    
             echo $this->input->post('firstname');    
            if($this->form_validation->run()){
                $firstName=$this->input->post('firstname');
                $lastName=$this->input->post('lastname');
                
                $this->load->model('message');
                $userTo=$this->message->check_user($firstName,$lastName);
                if($userTo==NULL){
                    redirect('welcome/members'); 
                }else{
                    $this->message->insert_new($userTo);
                    redirect('welcome/members');
                }               
            }else{
               redirect('welcome/members');
            }
        }    
        
        public function follow() {
            $this->load->model('user');  
            $id_followee=$this->input->post('idf');
            $id_follower=$this->session->userdata('id');
            $this->user->add_follow((int)$id_follower,(int) $id_followee); 
            redirect('welcome/members/'. $id_followee);
	}
        
        public function unfollow() {
            $this->load->model('user');  
            $id_followee=$this->input->post('idf');
            $id_follower=$this->session->userdata('id');
            $this->user->remove_follow((int)$id_follower,(int) $id_followee);
            redirect('welcome/members/'. $id_followee);
	}
        
        public function new_follow() {
            $this->load->model('user');  
            $id_followee=$this->input->post('idf');
            $id_follower=$this->session->userdata('id');
            $this->user->add_follow((int)$id_follower,(int) $id_followee); 
	}
        
         public function settings(){
            $this->load->model('user');
            $data = $this->user->get_this_user();
            $requests = $this->user->get_admin_requests();
            
            $this->load->view('header');
            $this->load->view('settings', array('id'=>$data['id'], 'type'=>$data['type'], 'admin_requests'=>$requests , 'status'=>$data['publicstatus'],'photos'=> $data['publicphotos']));
            
        }
        public function change_settings($selected_status, $selected_photos){
            
            $this->load->model('settings');
            $this->settings->save_changes($selected_status, $selected_photos);
            
            redirect('welcome/settings');
        }
        
        public function name_change_validation(){
            $this->form_validation->set_rules('firstname','First Name','trim|required|max_length[30]|xss_clean');
           
           $this->form_validation->set_rules('lastname','Last Name','trim|required|max_length[30]|xss_clean');
           if($this->form_validation->run()){
               $this->load->model('settings');
               $this->settings->change_name(); 
               
           }
           redirect('welcome/settings'); 
        }
        
        public function password_change_validation(){
            $this->form_validation->set_rules('password','Password','required|trim|xss_clean');
           $this->form_validation->set_rules('cpassword','Confirm Password','required|trim|xss_clean|matched[password]');
           if($this->form_validation->run()){
               $this->load->model('settings');
               $this->settings->change_password(); 
               
           }
           redirect('welcome/settings'); 
        }
        
        public function guest_login(){
            /*
             * guest za sada moze samo da vidi statuse drugih ljudi i nista drugo. Zato sto je guest i zato sto je saban.
             */
            $this->load->view('guest_header');
            
            $this->load->model('status');
                
            $data=$this->status->get_all();
                
            
            $this->load->view('guest_content',array('data'=>$data));
            $this->load->view('footer');
        }
        
        public function profile_guest_view($id){
            $this->load->view('guest_header');
            $this->load->model('user');
            $this->load->model('status');
            $user_data = $this->user->get_user($id);
            $user_status = $this->status->get_user_status($user_data['id']);
            
            
            $data = array(
                'user' => $user_data,
                'status' => $user_status,
            );
            $this->load->view('profile_guest', $data);
            $this->load->view('footer');
        }
        
        public function search(){
            $this->load->model('search');
            $this->load->model('status');
            $this->form_validation->set_rules('search_text','search_text','required');
            
                $this->load->view('header');
            if($this->form_validation->run()){
                $tag = $this->input->post('search_text');
                $data = $this->search->get_search($tag);
                if($this->session->userdata('is_logged_in')){
                    $this->load->view('hashtag',array('data'=>$data, 'my_id' => $this->session->userdata('id'), 'tag'=>$tag));
                }
                else
                { 
                    $this->load->view('guest_hashtags',array('data'=>$data, 'my_id' => $this->session->userdata('id'), 'tag'=>$tag));
                }
            }
            else{
                redirect('welcome/members');
            }
            
                $this->load->view('footer');
            
        }
        
        public function new_status(){
            $this->load->model('status');
            
            $this->form_validation->set_rules('status','status','required|trim|xss_clean');
            $img = NULL;
            if($this->form_validation->run()){                
                if (isset($_FILES['userfile'])) {
                    $config['upload_path'] = './assets/img';
                    $config['allowed_types'] = 'gif|jpg|png';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload())
                    {
                        $error = array('error' => $this->upload->display_errors());
                        $this->load->view('upload_form', $error);                   
                    }
                    else
                    {
                        $data = array('upload_data' => $this->upload->data());
                        $dat = $data['upload_data'];
                        $img = base_url().'assets/img/'.$dat['file_name'];
                    }
                }
                
                $this->status->upload_new($img);
                redirect('welcome/members');
            }else{
                redirect('welcome/members');
            }
        }
        
        public function request_admin() {
            $this->load->model('user');
            $this->user->request_admin_status();
            redirect('welcome/settings/');
        }
        
        public function aprove_admin($id) {
            $this->load->model('user');
            $this->user->aprove_admin($id);
            redirect('welcome/settings/');
        }
        
        public function decline_admin($id) {
            $this->load->model('user');
            $this->user->decline_admin($id);
            redirect('welcome/settings/');
        }

         public function list_all_users(){
            $this->load->model('user');

            $data=$this->user->list_all_users();

            $this->load->view('header');
            $this->load->view('users_list',array('data'=>$data));
            $this->load->view('footer');
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */