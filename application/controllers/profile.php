<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function index()
	{
		$this->load->view('profile');
                
	}
        
        public function members(){
            if($this->session->userdata('is_logged_in')){
                $this->load->view('header');
                
                $this->load->model('status');
                
                $data=$this->status->get_all();
                
                $this->load->view('members',array('data'=>$data));
                
                $this->load->view('footer');
            }else{
                redirect('welcome/restricted');
            }
        }
        
        public function new_status(){
            $this->form_validation->set_rules('status','status','required|trim|xss_clean');
            
            if($this->form_validation->run()){
                $this->load->model('status');
                $this->status->upload_new();
                 redirect('welcome/members');
            }else{
                redirect('welcome/members');
            }
        }
        
        public function restricted(){
            echo '<h1>You dont have access</h1>';
        }
        
        public  function login_validation(){
          
            
            $this->form_validation->set_rules('email','Email','required|trim|xss_clean|callback_validate_email');
            $this->form_validation->set_rules('password','Password','required|md5|trim');
            
            if($this->form_validation->run()){
                $this->load->model('user');
                $id=$this->user->getID();
                $data=array(
                    'id'=>$id,
                    'email'=>$this->input->post('email'),
                    'is_logged_in'=>1
                );
                $this->session->set_userdata($data);
                
                redirect('welcome/members');
            }else{
                $this->load->view('login');
            }
        }
        
        public function validate_email(){
            $this->load->model('user');
            
            if($this->user->can_log_in()){
                return true;
            }else{
                $this->form_validation->set_message('validate_email','Incorrect input');
                return false;
            }
        }
        
        public function logout(){
            $this->session->sess_destroy();
            redirect('welcome/index');
        }
        
        public function signup_validation(){
           $this->form_validation->set_rules('firstname','First Name','trim|required|max_length[30]|xss_clean');
           
           $this->form_validation->set_rules('lastname','Last Name','trim|required|max_length[30]|xss_clean');
           
           $this->form_validation->set_rules('email','Email','required|trim|xss_clean|valid_email|is_unique[user.email]');
           
           $this->form_validation->set_rules('password','Password','required|trim|xss_clean');
           $this->form_validation->set_rules('cpassword','Confirm Password','required|trim|xss_clean|matched[password]');
           
           $this->form_validation->set_message('is_unique',"That email address already exists.");
           
           if($this->form_validation->run()){
               $this->load->model('user');
               $this->user->add_user(); 
               $id=$this->user->getID();
                $data=array(
                    'id'=>$id,
                    'email'=>$this->input->post('email'),
                    'is_logged_in'=>1
                );
                $this->session->set_userdata($data);
                
                redirect('welcome/members');
               
               
           }else{
               $this->load->view('login');
               
           }
        }
        
        public function tempHome(){
            $this->load->view('header.php');
        }
        
        /**
         * TODO: Implement Home 
         */
        public function home(){
            echo "Home";
        }
        
        /**
         * TODO: Implement Profile
         */
        public function profile(){
            echo "Profile";
        }
        
        /**
         * TODO: Implement Messages
         */
        public function messages(){
            echo "Messages";
        }
        
        public function support(){
            $this->load->model('status');
            
            $id=$this->input->post('id');
            $this->status->incrementLikes($id);
            
            
            $this->load->model('supports');
            $this->supports->user_liked($id);
            
            $data=$this->status->get_all();
                //$this->load->view('members',array('data'=>$data));

        }
        
        public function support2(){
            $this->load->model('status');
            
            $id='8';
            $this->status->incrementLikes($id);
            
            
            $this->load->model('supports');
            $this->supports->user_liked($id);
            
            $data=$this->status->get_all();
                //$this->load->view('members',array('data'=>$data));
        }
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */